Source: pydispatcher
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-pytest,
               python3-setuptools
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Neil Muller <drnlmuller+debian@gmail.com>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/python-team/packages/pydispatcher.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pydispatcher
Homepage: https://mcfletch.github.io/pydispatcher/
Testsuite: autopkgtest-pkg-pybuild

Package: python3-pydispatch
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-pydispatch-doc
Description: Python 3 signal dispatching mechanism
 PyDispatcher provides the Python programmer with a
 multiple-producer-multiple-consumer signal-registration and routing
 infrastructure for use in multiple contexts. The mechanism of PyDispatcher
 started life as a highly rated recipe in the Python Cookbook. The project
 aims to include various enhancements to the recipe developed during use in
 various applications.
 .
 This package contains the Python 3 version of PyDispatcher.

Package: python-pydispatch-doc
Architecture: all
Depends: ${misc:Depends}
Section: doc
Multi-Arch: foreign
Description: documentation for python3-pydispatch
 PyDispatcher provides the Python programmer with a
 multiple-producer-multiple-consumer signal-registration and routing
 infrastructure for use in multiple contexts. The mechanism of PyDispatcher
 started life as a highly rated recipe in the Python Cookbook. The project
 aims to include various enhancements to the recipe developed during use in
 various applications.
 .
 This package contains the documentation for PyDispatcher. It covers
 the Python 3 versions.
